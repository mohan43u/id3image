#ifndef __ID3IMAGE__
#define __ID3IMAGE__

#include <iostream>
#include <id3/tag.h>
#include <getopt.h>

using namespace std;

class id3image: public ID3_Tag
{
  ID3_Frame *frame;
  ID3_Field *field;
 public:
  id3image(void) {};
  ~id3image(void) {};
  bool have_image(void);
  int save_image(const char *imagepath);
  int load_image(const char *imagepath);
};

#endif /* __ID3IMAGE__ */
