#include <id3image.h>

bool id3image::have_image(void)
{
  frame = Find(ID3FID_PICTURE); //searching for APIC frame
  if(frame == NULL)
    return(false);
  else
    return(true);
}

int id3image::save_image(const char *imagepath)
{
  if(have_image())
    {
      field = frame->GetField(ID3FN_DATA);
      field->ToFile(imagepath);
      cout << "ID3FN_DATA written into " << imagepath << " from " << GetFileName() << endl;
      return(0);
    }
  else
    {
      cerr << "no ID3FID_PICTURE in " << GetFileName() << endl;
      return(1);
    }

}

int id3image::load_image(const char *imagepath)
{
  if(have_image() == false)
    {
      frame = new ID3_Frame(ID3FID_PICTURE);
      AttachFrame(frame);

    }

  field = frame->GetField(ID3FN_MIMETYPE);
  int len = strlen(imagepath);
  char mime[512];
  memset(mime, 0, 512); // clearing mime
  strcat(mime, "image/");
  // taking extension from imagepath assuming it is 3 bytes
  const char *ext = imagepath + (len - 3);
  strcat(mime, ext);
  field->Set(mime);

  field = frame->GetField(ID3FN_PICTURETYPE);
  field->Set((uint32) 03); // setting PICTURETYPE as 'Cover (front)'

  field = frame->GetField(ID3FN_DATA);
  field->FromFile(imagepath);
  Update();

  cout << "ID3FN_DATA loaded from " << imagepath << " into " << GetFileName() << endl;

  return(0);
}
