#include <id3image.h>

int main(int argc, char *argv[])
{
  int cmd = 0; // by default save the image embedded inside the mp3file
  char *imagepath = NULL;
  char opt = '\0';
  char usage[] = "[usage] id3image [-l|--load] -i|--image imagepath mp3file[..]";
  struct option longopts[] = {{"load", 0, NULL, 'l'},
			      {"image", 1, NULL, 'i'},
			      {"help", 0, NULL, 'h'}};

  if(argc < 2)
    {
      cerr << usage << endl;
      exit(EXIT_FAILURE);
    }

  while((opt = getopt_long(argc, argv, "li:h", longopts, NULL)) != -1)
    {
      switch(opt)
	{
	case 'l': cmd = 1;
	  break;
	case 'i': imagepath = optarg;
	  break;
	case 'h':
	  {
	    cout << usage << endl;
	    exit(EXIT_FAILURE);
	  }
	  break;
	default:
	  {
	    cout << "unknown option " << opt << endl;
	    exit(EXIT_FAILURE);
	  }
	}
    }
  if(imagepath == NULL)
    {
      cerr << usage << endl;
      exit(EXIT_FAILURE);
    }

  int iter = optind;
  int returncode = 0;

  while(iter < argc && returncode == 0)
    {
      id3image image;
      image.Link(argv[iter]);
      if(cmd)
	returncode = image.load_image(imagepath);
      else
	returncode = image.save_image(imagepath);
      iter++;
    }

  return(returncode);
}
